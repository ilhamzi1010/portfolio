"use client";
import { useEffect, useRef, useState } from "react";
import Portfolios from "@/public/assets/data-portfolio/portfolio.json";
import Image from "next/image";

export const TabsComponent = () => {
  const [selectedTab, setSelectedTab] = useState(0);

  const buttonRef = useRef();

  useEffect(() => {
    buttonRef.current.focus();
  }, []);

  return (
    <>
      <div className="menu-portfolio flex justify-center">
        <ul className="w-[675px] h-[40px] mx-auto text-center text-white mb-7">
          <li className="grid grid-cols-4 gap-4 content-center">
            {Portfolios.map((portfolio, index) => (
              <button
                ref={index === 0 ? buttonRef : null}
                key={index}
                onClick={() => setSelectedTab(index)}
                className="outline-none w-full text-base hover:border-2 text-center focus:border-2 rounded-lg"
              >
                {portfolio.title}
              </button>
            ))}
          </li>
        </ul>
      </div>
      <div>
        {Portfolios.map((portfolio, index) => {
          return (
            <div
              className={`${selectedTab === index ? "columns-4 gap-3 w-[1080px] mx-auto space-y-3 pb-28 " : "hidden"}`}
              key={portfolio.id}
            >
              {portfolio.graphicDesign &&
                portfolio.graphicDesign.map((data) => (
                  <div key={data.imageid} className="bg-gray break-inside-avoid">
                    <Image src={data.image} className="h-auto max-w-full rounded-lg" width={1920} height={1080} alt="" />
                  </div>
                ))}
              {portfolio.product3d &&
                portfolio.product3d.map((data) => (
                  <div key={data.imageid} className="bg-gray break-inside-avoid">
                    <Image src={data.image} className="h-auto max-w-full rounded-lg" width={1920} height={1080} alt="" />
                  </div>
                ))}
            </div>
          );
        })}
      </div>
    </>
  );
};

export default TabsComponent;
