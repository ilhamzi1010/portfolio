"use client";
import Image from "next/image";
import { bgProduk } from "../../../public/assets/images";
import Link from "next/link";
import { useEffect, useState } from "react";
import Navbar from "@/app/utilities/Navbar";

export const CarouselBanner = ({ 
    children: items,
    autoSlide = false,
    autoSlideInterval = 3000,
}) => {
  const [slide, setSlide] = useState(0);

  const prev = () =>
    setSlide((slide) => (slide === 0 ? items.length - 1 : slide - 1));
  const next = () =>
    setSlide((slide) => (slide === items.length - 1 ? 0 : slide + 1));

  useEffect(() => {
    if (!autoSlide) return
    const SlideInterval = setInterval(next, autoSlideInterval)
    return () => clearInterval(SlideInterval)
}, [])

  return (
    <>
    <Navbar />
      <div className="carousel overflow-hidden">
        <div
          className="gambar flex transition-transform ease-in-out duration-1000"
          style={{ transform: `translateX(-${slide * 100}%)` }}
        >
          {items}
        </div>
      </div>
        <div className="carousel_button w-[300px] h-[40px] inline-flex items-center justify-center">
          <button
            onClick={prev}
            type="button"
            className=" z-30 h-full cursor-pointer group focus:outline-none mx-5"
            data-carousel-prev
          >
            <div className="origin-top-left -rotate-180 w-[38px] h-[38px] ">
              <div className="w-[38px] h-[38px] inline-flex items-center justify-center left-0 top-0 absolute origin-top-left -rotate-180 rounded-[10px] border border-white">
                <svg
                  className="w-4 h-4 text-white dark:text-gray-800 rtl:rotate-180"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 6 10"
                >
                  <path
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="M5 1 1 5l4 4"
                  />
                </svg>
              </div>
            </div>
            <span className="sr-only">Previous</span>
          </button>

          <div className="flex item-center ">
            <div className="h-full w-[100px] flex justify-evenly">
              {items.map((_, i) => (
                <button
                  type="button"
                  className={`transition-all w-[11px] h-[11px] rounded-full
                                        ${
                                          slide === i 
                                            ? "bg-white w-[15px] h-[15px] rounded-full border-2 border-white"
                                            : "bg-transparent rounded-full border-2 border-white"
                                        }`}
                  aria-current="true"
                  aria-label="Slide 1"
                  data-carousel-slide-to="0"
                ></button>
              ))}
            </div>
          </div>

          <button
            onClick={next}
            type="button"
            className="z-30 h-full cursor-pointer group focus:outline-none mx-5"
            data-carousel-next
          >
            <div className="origin-top-left -rotate-180 w-[38px] h-[38px]">
              <div className="w-[38px] h-[38px] inline-flex items-center justify-center left-0 top-0 absolute origin-top-left -rotate-180 rounded-[10px] border border-white">
                <svg
                  className="w-4 h-4 text-white dark:text-gray-800 rtl:rotate-180"
                  aria-hidden="true"
                  xmlns="http://www.w3.org/2000/svg"
                  fill="none"
                  viewBox="0 0 6 10"
                >
                  <path
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    d="m1 9 4-4-4-4"
                  />
                </svg>
              </div>
            </div>
          </button>
        </div>
        <div className="content">
          <h1 className=" text-white font-sans">PORTFOLIO</h1>
          <Link
            href="/portfolio"
            className="show-more relative flex item-center"
          >
            <div className="w-[37px] h-[37px] absolute rounded-[10px] border border-white"></div>
            <span className="text-white font-sans">Show more</span>
          </Link>
        </div>

      {/* <div id="default-carousel" data-carousel="slide">

                <div className="carousel overflow-hidden">
                    <div className="item hidden duration-700 ease-in-out" data-carousel-item>
                        <Image src={bgProduk} className="gambar -translate-x-1/2 -translate-y-1/2" alt="" />
                        {items}
                    </div>
                </div>
                <div className="content">
                    <h1 className=" text-white font-sans">PORTFOLIO</h1>
                    <Link href="/portfolio" className="show-more relative flex item-center">
                        <div className="w-[37px] h-[37px] absolute rounded-[10px] border border-white"></div>
                        <span className="text-white font-sans">Show more</span>
                    </Link>
                </div>

                <div className="carousel_button w-[300px] h-[40px] inline-flex items-center justify-center" >
                    <button onClick={prev} type="button" className=" z-30 h-full cursor-pointer group focus:outline-none mx-5" data-carousel-prev>
                        <div className="origin-top-left -rotate-180 w-[38px] h-[38px] ">
                            <div className="w-[38px] h-[38px] inline-flex items-center justify-center left-0 top-0 absolute origin-top-left -rotate-180 rounded-[10px] border border-white">
                                <svg className="w-4 h-4 text-white dark:text-gray-800 rtl:rotate-180" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
                                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5 1 1 5l4 4"/>
                                </svg>
                            </div>
                        </div>
                        <span className="sr-only">Previous</span>
                    
                    </button>

                    <div className="flex item-center ">
                        <div className="h-full w-[100px] flex item-center justify-between ">
                            <button type="button" className="w-[11px] h-[11px] bg-white rounded-full" aria-current="true" aria-label="Slide 1" data-carousel-slide-to="0"></button>
                            <button type="button" className="w-[11px] h-[11px] rounded-full border border-white" aria-current="false" aria-label="Slide 2" data-carousel-slide-to="1"></button>
                            <button type="button" className="w-[11px] h-[11px] rounded-full border border-white" aria-current="false" aria-label="Slide 3" data-carousel-slide-to="2"></button>
                            <button type="button" className="w-[11px] h-[11px] rounded-full border border-white" aria-current="false" aria-label="Slide 4" data-carousel-slide-to="3"></button>
                        </div>
                    </div>
                    
                    <button onClick={next} type="button" className="z-30 h-full cursor-pointer group focus:outline-none mx-5" data-carousel-next>
                        <div className="origin-top-left -rotate-180 w-[38px] h-[38px]">
                            <div className="w-[38px] h-[38px] inline-flex items-center justify-center left-0 top-0 absolute origin-top-left -rotate-180 rounded-[10px] border border-white">
                                <svg className="w-4 h-4 text-white dark:text-gray-800 rtl:rotate-180" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 6 10">
                                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 9 4-4-4-4"/>
                                </svg>
                            </div>
                        </div>
                    </button>
                </div>    
            </div> */}
    </>
  );
};

export default CarouselBanner;
