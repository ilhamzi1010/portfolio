import type { Metadata } from 'next'
import { Poppins } from 'next/font/google'
import './globals.css'


const poppins = Poppins({weight: '200' ,subsets: ['latin'] })

export const metadata: Metadata = {
  
  title: 'Portfolio Ilham',
  description: 'Generated by create next app',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en" className='scroll-smooth'>
      <body className={poppins.className}>{children}</body>
    </html>
  )
}
