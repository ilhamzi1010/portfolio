"use client"
import { logo } from "@/public/assets";
import Image from "next/image";
import Link from "next/link";
import { usePathname } from "next/navigation";

const navLinks = [
  { name: "Home", href: "/" },
  { name: "Portfolio", href: "/portfolio" },
  { name: "About", href: "/about" },
  { name: "Contact", href: "/contact" },
];

const Navbar = () => {
  const pathname = usePathname();

  return (
    <>
      <header className="bg-transparent z-0">
        <nav
          className="mx-auto flex max-w-7xl item-center justify-between p-6 lg:px-2 "
          aria-label="Global"
        >
          <div className="flex lg:flex-1">
            <Link href="/" className="-m-1.5 p-1.5">
              <Image className="h-8 w-auto" src={logo} alt="" />
            </Link>
          </div>
          <div className="flex lg:hidden">
            <button
              type="button"
              className="-m-2.5 inline-flex items-center justify-center rounded-md p-2.5 text-gray-700"
            >
              <span className="sr-only">Open main menu</span>
              <svg
                className="h-6 w-6"
                fill="none"
                viewBox="0 0 24 24"
                stroke-width="1.5"
                stroke="currentColor"
                aria-hidden="true"
              >
                <path
                  stroke-linecap="round"
                  stroke-linejoin="round"
                  d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
                />
              </svg>
            </button>
          </div>
          <ul >
            <li className="hidden lg:flex lg:gap-x-16">
              {navLinks.map((link) => {
                
                return (
                  <Link
                    href={link.href}
                    key={link.name}
                    className={`${pathname === link.href
                        ? "text-white font-bold border-b-2 border-white mx-1.5 sm:mx-6 "
                        : "text-white font-medium border-b-2 border-transparent hover:text-white dark:hover:text-white hover:border-white mx-1.5 sm:mx-6"}`
                      
                    }
                  >
                    {link.name}
                  </Link>
                );
              })}
            </li>
          </ul>
        </nav>
      </header>
    </>
  );
};

export default Navbar
