import { logo } from "@/public/assets"
import Image from "next/image"


const Footer = () => {
    return(
        <>
          <footer className="footer flex bg-[#BAD3CB] w-full h-full">
            <div className="logo ml-44">
                <a href="#" className="-m-1.5">
                    <Image className='h-8 w-auto' src={logo} alt="" />
                </a>
            </div>
            <span className="copyright block font-sans text-md text-[#617871] sm:text-center dark:text-gray-400">copyright © 2024 <a href="#" className="hover:underline">Ilhamzi</a>.Studio</span>
          </footer>
        </>
    )
} 

export default Footer