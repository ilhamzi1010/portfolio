'use client'
import Image from "next/image";
import Navbar from "../utilities/Navbar";
import { bgProduk, JVD1, profil, studio1 } from "@/public/assets";
import Footer from "../utilities/Footer";
import CarouselBanner from "../components/Carousel";



export const About = () => {
  return (
    <>
      <Navbar />
      <div className="about bg-[#BAD3CB] z-1 h-screen w-screen -mt-20 flex justify-center">
        
              <h1>About Me</h1>
              <div className="shadow">

                <h1>About Me</h1>
              </div>
        
        <div className="z-0 grid grid-cols-2 content-center mt-10 w-[1055px]">
          <div className="gambar flex">
            <Image className="h-auto max-w-full" src={profil} alt="" />
          </div>
          <div className="flex">
            <article className="content-end -ml-36">
              <h2 className="font-bold text-xl my-3">Hello there, i’m Ilham</h2>
              <p className="font-sans">
                Lorem ipsum dolor sit amet consectetur. Tempor id eget in turpis
                nisl morbi sed. Amet diam faucibus quis amet amet maecenas
                ultricies. Tempor consectetur in non sem faucibus sed vitae.
                Scelerisque vulputate ac aliquam in neque sed facilisis
                eleifend. Eget risus et augue pellentesque sapien viverra
                placerat.
              </p>
            </article>
          </div>
        </div>
        
      </div>
        
        
 
      <Footer />
    </>
  );
};

export default About;
