import Navbar from "../utilities/Navbar";
import TabsComponent from "../components/tabsComponent";
import Image from "next/image";
import {
  JVD1,
  bgProduk,
  handwash1,
  handwash2,
  handwash3,
  scenery1,
  scenery2,
  webJoycell,
  webJoyzetox,
} from "@/public/assets";
import Footer from "../utilities/Footer";

const Portfolio = () => {
  return (
    <>
      <Navbar />
      <div className="banner w-full h-[670px] bg-[#BAD3CB] -mt-20">
        <div className="flex justify-center">
          <h1>Portfolio</h1>
          <div className="shadow flex justify-center pl-20">
            <h1>Portfolio</h1>
          </div>
        </div>
      </div>
      <TabsComponent items={items} />
      <Footer />
    </>
  );
};

export default Portfolio;

const items = [
  {
    title: "All Product",
    content: (
      <div className="columns-4 gap-3 w-[1080px] mx-auto space-y-3 pb-28">
        <div className="bg-gray break-inside-avoid">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={handwash3}
            alt=""
          />
        </div>
        <div className="bg-gray break-inside-avoid">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={scenery2}
            alt=""
          />
        </div>
        <div className="bg-gray break-inside-avoid">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={bgProduk}
            alt=""
          />
        </div>
        <div className="bg-gray break-inside-avoid">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={handwash1}
            alt=""
          />
        </div>
        <div className="bg-gray break-inside-avoid">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={scenery1}
            alt=""
          />
        </div>
        <div className="bg-gray break-inside-avoid">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={handwash3}
            alt=""
          />
        </div>
        <div className="bg-gray break-inside-avoid">
          <Image className="h-auto max-w-full rounded-lg" src={JVD1} alt="" />
        </div>
      </div>
    ),
  },
  {
    title: "Web Devlopment",
    content: (
      <div className="grid grid-cols-3 gap-5 w-[1080px] mx-auto pb-28">
        <div className="bg-gray break-inside-avoid flex justify-center">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={webJoycell}
            alt=""
          />
          <div className="backdrop-blur-sm bg-white/30  absolute mt-48 w-[322px] h-[136px] rounded-lg">
            <div className="web-text my-5 mx-3">
                <h2>Joycell Landing Page</h2>
                <p className="font-sans text-xs">
                Lorem ipsum dolor sit amet consectetur. Viverra libero ligula
                vulputate integer. Amet ac a aenean molestie. Tellus nibh est ut
                neque. Porttitor ultrices sed arcu suspendisse aenean.
                </p>
            </div>
          </div>
        </div>
        <div className="bg-gray break-inside-avoid flex justify-center">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={webJoyzetox}
            alt=""
          />
          <div className="backdrop-blur-sm bg-white/30  absolute mt-48 w-[322px] h-[136px] rounded-lg">
            <div className="web-text my-5 mx-3">
                <h2>Joycell Landing Page</h2>
                <p className="font-sans text-xs">
                Lorem ipsum dolor sit amet consectetur. Viverra libero ligula
                vulputate integer. Amet ac a aenean molestie. Tellus nibh est ut
                neque. Porttitor ultrices sed arcu suspendisse aenean.
                </p>
            </div>
          </div>
        </div>
        <div className="bg-gray break-inside-avoid flex justify-center">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={webJoyzetox}
            alt=""
          />
          <div className="backdrop-blur-sm bg-white/30  absolute mt-48 w-[322px] h-[136px] rounded-lg">
            <div className="web-text my-5 mx-3">
                <h2>Joycell Landing Page</h2>
                <p className="font-sans text-xs">
                Lorem ipsum dolor sit amet consectetur. Viverra libero ligula
                vulputate integer. Amet ac a aenean molestie. Tellus nibh est ut
                neque. Porttitor ultrices sed arcu suspendisse aenean.
                </p>
            </div>
          </div>
        </div>
        <div className="bg-gray break-inside-avoid flex justify-center">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={webJoyzetox}
            alt=""
          />
          <div className="backdrop-blur-sm bg-white/30 mt-48 absolute  w-[322px] h-[136px] rounded-lg">
            <div className="web-text my-5 mx-3">
                <h2>Joyzetox Landing Page</h2>
                <p className="font-sans text-xs">
                Lorem ipsum dolor sit amet consectetur. Viverra libero ligula
                vulputate integer. Amet ac a aenean molestie. Tellus nibh est ut
                neque. Porttitor ultrices sed arcu suspendisse aenean.
                </p>
            </div>
          </div>
        </div>
        
      </div>
    ),
  },
  {
    title: "Design Graphic",
    content: (
      <div className="columns-4 gap-3 w-[1080px] mx-auto space-y-3 pb-28">
        <div className="bg-gray break-inside-avoid">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={bgProduk}
            alt=""
          />
        </div>
        <div className="bg-gray break-inside-avoid">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={scenery2}
            alt=""
          />
        </div>
        <div className="bg-gray break-inside-avoid">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={scenery1}
            alt=""
          />
        </div>
        <div className="bg-gray break-inside-avoid">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={handwash1}
            alt=""
          />
        </div>
        <div className="bg-gray break-inside-avoid">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={handwash3}
            alt=""
          />
        </div>
        <div className="bg-gray break-inside-avoid">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={handwash3}
            alt=""
          />
        </div>
        <div className="bg-gray break-inside-avoid">
          <Image className="h-auto max-w-full rounded-lg" src={JVD1} alt="" />
        </div>
      </div>
    ),
  },
  {
    title: "3D Product",
    content: (
      <div className="columns-4 gap-3 w-[1080px] mx-auto space-y-3 pb-28">
        <div className="bg-gray break-inside-avoid">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={scenery2}
            alt=""
          />
        </div>
        <div className="bg-gray break-inside-avoid">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={handwash3}
            alt=""
          />
        </div>
        <div className="bg-gray break-inside-avoid">
          <Image className="h-auto max-w-full rounded-lg" src={JVD1} alt="" />
        </div>
        <div className="bg-gray break-inside-avoid">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={handwash1}
            alt=""
          />
        </div>
        <div className="bg-gray break-inside-avoid">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={bgProduk}
            alt=""
          />
        </div>
        <div className="bg-gray break-inside-avoid">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={handwash3}
            alt=""
          />
        </div>
        <div className="bg-gray break-inside-avoid">
          <Image
            className="h-auto max-w-full rounded-lg"
            src={scenery1}
            alt=""
          />
        </div>
      </div>
    ),
  },
];
