"use client";
import Navbar from "@/app/utilities/Navbar";
import Link from "next/link";
import Portfolios from "@/public/assets/data-portfolio/portfolio.json";
import Image from "next/image";

const Modeling = () => {


  return (
    <>
      <div className=" background-cover"></div>
      <div className="sticky top-0">
        <Navbar />
      </div>
      <div className="max-h-full -mt-20 pb-24">
        <div className="grid grid-cols-2 gap-4 w-full h-full">
          <div className="image-port-3d left-part h-full w-full mt-24">
            {Portfolios.map((portfolio) => {
              // console.log(portfolio.product3d);
              
              return (
                <div
                  className="columns-2 gap-3 w-[880px] mx-3 space-y-3 pb-28"
                  key={portfolio.id}
                >
                  {portfolio.product3d && portfolio.product3d.map((data) => 
                      <div key={data.imageid} className="w-full">
                        <Image
                          src= {data.image}
                          className="w-full h-full rounded-2xl"
                          width={1920}
                          height={1080}
                          alt="image"
                        />
                        
                      </div>
                  )}
                </div>
              );
            })}
          </div>
          <div className="page-right-port right-part h-[745px] mt-[100px] ">
            <div className="w-[37px] h-[37px]">
              <Link href="/">
                <div className=" w-[37px] h-[37px] rounded-[10px] border-2 border-white content-center">
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    fill="none"
                    viewBox="0 0 24 24"
                    strokeWidth={1.5}
                    stroke="white"
                    className="w-6 h-6 m-1 rotate-180"
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M17.25 8.25 21 12m0 0-3.75 3.75M21 12H3"
                    />
                  </svg>
                </div>
              </Link>
              <div className="portfolio-3D h-[250px] mt-[50px]">
                <h1>3D</h1>
                <h1 className="-mt-16">Product</h1>
                <div className="opacity-10 -mt-80 pl-3">
                  <h1>3D</h1>
                  <h1 className="-mt-16">Product</h1>
                </div>
              </div>
              <div className=" h-52 w-[620px] mt-20">
                <h1 className="text-[#617871] text-2xl font-bold font-sans my-3">
                  3D Modeling & Photography Product
                </h1>
                <p className="text-[#617871] text-lg font-medium font-sans">
                  A selection of personal work in companies and freelancers. I
                  continue to practice and produce work to increase the value of
                  a company, in promoting a product. All work is done in Blender
                  + Cinema 4D and rendered in Redshift.
                </p>
              </div>
              <div className=" mt-10 h-20 w-[620px] flex justify-between">
                <Link href="/portfolio/Graphic-Design">
                  <div className=" w-[175px] h-[40px] rounded-[50px] border-2 border-white flex justify-evenly">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="white"
                      className="w-6 h-6 m-1 rotate-180"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M17.25 8.25 21 12m0 0-3.75 3.75M21 12H3"
                      />
                    </svg>
                    <span className="text-white font-medium font-sans m-1">
                      Graphic Design
                    </span>
                  </div>
                </Link>
                <Link href="/">
                  <div className=" w-[205px] h-[40px] rounded-[50px] border-2 border-white flex justify-evenly">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      fill="none"
                      viewBox="0 0 24 24"
                      strokeWidth={1.5}
                      stroke="white"
                      className="w-6 h-6 m-1"
                    >
                      <path
                        strokeLinecap="round"
                        strokeLinejoin="round"
                        d="M17.25 8.25 21 12m0 0-3.75 3.75M21 12H3"
                      />
                    </svg>
                    <span className="text-white font-medium font-sans m-1">
                      Web Development
                    </span>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Modeling;
