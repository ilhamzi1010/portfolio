import bgProduk from "./produk/Joyomega_display.png";
import thumbnail_1 from "./thumbnail/thumbnail_Joyomega.jpg";
import thumbnail_2 from "./thumbnail/thumbnail_sosmed.png";
import thumbnail_3 from "./thumbnail/thumbnail_ Website.jpg";
import thumbnail_4 from "./thumbnail/thumbnail_contact.jpg";
import thumbnail_5 from "./thumbnail/thumbnail_weball.png";
import logo from "./logo/il.png";
import ps_logo from "./logo/ps-logo.png";
import blender from "./logo/Blender-logo.png";
import ai from "./logo/il-logo.png";
import figma from "./logo/figma-logo.png";
import react from "./logo/react-logo.png";
import php from "./logo/php-logo.png";
import html from "./logo/html-logo.png";
import css from "./logo/css_logo.png";
import JVD1 from "./produk/Display 1 JVD new.jpg";
import studio1 from "./produk/studio_1.jpg";
import scenery1 from "./produk/scenery_1.jpg";
import scenery2 from "./produk/scenery_2.jpg";
import handwash1 from "./produk/Handwash1.jpg";
import handwash2 from "./produk/Handwash2.jpg";
import handwash3 from "./produk/Handwash3.jpg";
import webJoycell from "./produk/webJoycell.png";
import webJoyzetox from "./produk/webJoyzetox.png";
import profil from "./profil/profil.png";



// icon
// import location from "./icon/location_icon.svg";

export {
    bgProduk,
    logo,
    ps_logo,
    ai,
    blender,
    figma,
    react,
    php,
    html,
    css,
    thumbnail_1,
    thumbnail_2,
    thumbnail_3,
    thumbnail_4,
    thumbnail_5,
    JVD1,
    scenery1,
    scenery2,
    handwash1,
    handwash2,
    handwash3,
    profil,
    studio1,
    webJoycell,
    webJoyzetox,    

    // icon
    // location,
}

